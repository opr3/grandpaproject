﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiaSkin : MonoBehaviour
{
   
    
    
        public Material material1;
        public Material material2;
        public Material material3;
        public Material material4;
        public Material material5;

        public Renderer rend;
        public GameObject player;
        public int skin;


        void Start()
        {

            skin = 1;

            player = GameObject.FindGameObjectWithTag("GrandpaCar");
            rend = player.GetComponent<Renderer>();
            rend.enabled = true;

        }


        void Update()
        {


            if (PlayerPrefs.GetInt("ModelloMacchina1") >0)
            {
                skin = skin + 1;
                ChangeSkin();
            }
            if (skin >= 6)
            {
                skin = 1;
                ChangeSkin();
            }
            DontDestroyOnLoad(this);

        }

        void ChangeSkin()
        {
            if (skin == 1)
            {
                rend.material = material1;
            }
            if (skin == 2)
            {
                rend.material = material2;
            }
            if (skin == 3)
            {
                rend.material = material3;
            }
            if (skin == 4)
            {
                rend.material = material4;
            }
            if (skin == 5)
            {
                rend.material = material5;
            }


        }
    }

