﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    public AudioClip moneyFx; // suono quando prendi moneta
    public AudioClip bucaFx; // suono quando prendi buca
    public AudioClip gameoverFx; // suono gameover
    public AudioClip immortalFx; // suono distruggi macchina
    public AudioClip rageFx; // suono rage bar piena

    public GameObject particle;
    public GameObject smallExplosion;
    public GameObject plasmaExplosion;   
    public float speed = 0;
    public Material material;

    private int Corsia = 0;
    
    public GameObject grandpaCar;
    Vector3 pos;

    public bool partito;
    Rigidbody rb;
    public bool gameOver;
    private bool macchinaColpita;
    public static CarController current;
    public bool immortal;


    private void Awake()
    {
        rb = GetComponent<Rigidbody>(); // Mi serve per lavorare sul rigidbody della macchina da qui
        if (current == null)
        {
            current = this;
        }
    }

    void Start()
    {
        partito = false;
        gameOver = false;
        macchinaColpita = false;
        


    }


    void Update()
    {
        if (!partito)
        {

            if (Input.GetMouseButtonDown(0))
            {
                
                rb.velocity = new Vector3(0, 0, speed);
                speed = 3;
                partito = true;
                SpawnerPiattaforma.current.CominciaSpawn();

                GameManager.current.StartGame();
            }

        }

        if (!Physics.Raycast(transform.position, Vector3.down, 1f) || macchinaColpita)
        {

            gameOver = true;
            rb.velocity = new Vector3(0, -25f, 0);

            Camera.main.GetComponent<CameraController>().gameOver = true;

            SpawnerPiattaforma.current.gameOver = true;

            GameManager.current.GameOver();
        }
        else if (speed <= 0)
        {
            gameOver = true;
            rb.velocity = new Vector3(0, -25f, 0);

            Camera.main.GetComponent<CameraController>().gameOver = true;

            SpawnerPiattaforma.current.gameOver = true;

            GameManager.current.GameOver();
        }



        if (Input.GetMouseButtonDown(0) && gameOver != true) // 0 = tasto sx , 1= tasto dx
        {
            CambiaDirezione();//OGNI VOLTA che l'user tocca lo smartphone la macchina cambierà direzione
        }
        if (ScoreManager.current.score > 25 && ScoreManager.current.score < 50)
        {
            CarController.current.speed = 4;
            
        }
        else if (ScoreManager.current.score >= 50 && ScoreManager.current.score < 75)
        {
            CarController.current.speed = 5;
 
        }
        

        else if (ScoreManager.current.score >= 75 && ScoreManager.current.score < 100)
        {
            CarController.current.speed = 6;

        }
        else if (ScoreManager.current.score >= 100 && ScoreManager.current.score < 125)
        {
            CarController.current.speed = 8;
            
        }
        else if (ScoreManager.current.score >= 125 && ScoreManager.current.score < 150)
        {
            CarController.current.speed = 10;

        }
        else if (ScoreManager.current.score >= 150 && ScoreManager.current.score < 175)
        {
            CarController.current.speed = 12;

        }
        else if (ScoreManager.current.score >= 175 && ScoreManager.current.score < 200)
        {
            CarController.current.speed = 14;

        }
        else if (ScoreManager.current.score >= 200)
        {
            CarController.current.speed = 16;

        }
    }

    void CambiaDirezione()
    {

        if (Corsia == 0)// X=0 corsia di sx
        {
            rb.transform.Rotate(0, 25, 0);
            rb.velocity = new Vector3(3f, 0, speed); // va a dx
            Invoke("SgommataDX", 0.25f);

            Corsia++; // Diventa Corsia di DX

        }
        else if (Corsia == 1)
        {
            rb.transform.Rotate(0, -25, 0);
            rb.velocity = new Vector3(-3f, 0, speed); //va a sx
            Invoke("SgommataSX", 0.25f);

            Corsia--; // Torna sulla Corsia di SX

        }

    }

    private void OnTriggerEnter(Collider other) //Quando la palla tocca la moneta , essa viene distrutta
    {
        if (other.gameObject.tag == "Gold")
        {
            ScoreManager.current.MonetaScore();
            
            GameObject particella = Instantiate(particle, other.gameObject.transform.position, Quaternion.identity) as GameObject;
            AudioManager.current.playSound(moneyFx);
            Destroy(other.gameObject);
            Destroy(particella, 1f);
            
           
        }
        else if (other.gameObject.tag == "Buca")
        {
            Destroy(other.gameObject, 2f);
            RageBarScript.current.rabbia++;
            AudioManager.current.playSoundLow(bucaFx); // suono quando prendi buca
        }
        else if (other.gameObject.tag == "StradaEsterna")
        {

            gameOver = true;
            rb.velocity = new Vector3(0, -25f, 0);
            AudioManager.current.playSound(rageFx);
            Camera.main.GetComponent<CameraController>().gameOver = true;
            
            SpawnerPiattaforma.current.gameOver = true;

            GameManager.current.GameOver();
         }

        else if (other.gameObject.tag == "Enemy" && immortal == false)
        {
            macchinaColpita = true;
            BoxCollider.Instantiate(other, other.gameObject.transform.position, Quaternion.identity);
            GameObject esplosione = Instantiate(smallExplosion, other.gameObject.transform.position, Quaternion.identity) as GameObject;
            AudioManager.current.playSoundHigh(gameoverFx);
            Destroy(esplosione, 1f);
            speed = -10f;
        }

        else if (other.gameObject.tag == "Enemy" && immortal == true)
        {

            Destroy(other.gameObject, 0.01f);
            GameObject esplosione = Instantiate(plasmaExplosion, other.gameObject.transform.position, Quaternion.identity) as GameObject;
            AudioManager.current.playSound(immortalFx);
            Destroy(esplosione, 1f);
            //aggiungo effetto
            //richiamo void x sec
            RageBarScript.current.rabbia = 0;
            immortal = false;
        }

    }
   
    private void SgommataDX()
    {

        rb.transform.Rotate(0, -25, 0);

    }

    private void SgommataSX()
    {

        rb.transform.Rotate(0, 25, 0);

    }
    public void immortale()
    {

        if (RageBarScript.current.rabbia < 10)
        {
            immortal = false;
        }
        else
            immortal = true;
           

    }
}

