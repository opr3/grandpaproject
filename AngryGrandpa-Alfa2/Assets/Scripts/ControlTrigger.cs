﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlTrigger : MonoBehaviour
{
    public GameObject EnemyCar;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "GrandpaCar")
        {
           
            /* chiamo la void per distruggere la piattaforma per non intasare la memoria */
            Destroy(transform.parent.gameObject, 2f); //distrugge il genitore di control trigger dopo 0.5s
            Destroy(EnemyCar, 0.5f);
        }
    }
}
