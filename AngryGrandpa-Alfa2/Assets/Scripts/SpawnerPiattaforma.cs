﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerPiattaforma : MonoBehaviour
{
    public static SpawnerPiattaforma current;

    public GameObject RoadTile;
   
    public GameObject Gold;
    public GameObject Car;
    public GameObject Car1;
    public GameObject Car2;
    public GameObject Buca;
    public int spawnCarRateSx;
    public int spawnCarRateDx;

    Vector3 ultimaPos;
    Vector3 pos;

    float size;
  
    public bool gameOver;

    private void Awake()
    {
        if (current == null)
        {
            current = this;
        }
    }
    // Use this for initialization
    void Start()
    {
        ultimaPos = RoadTile.transform.position;
        size = RoadTile.transform.localScale.z;
        spawnCarRateSx = 400;
        spawnCarRateDx = 600;

        

        for (int i = 0; i < 30; i++) // creo 25 piattaforme iniziali e il resto dopo il primo click dell user
        {
            SpawnTile();
           
        }
        CominciaSpawn();


    }

    // Update is called once per frame
    void Update()
    {
        if (gameOver)
            CancelInvoke("SpawnTile");
    }

    public void CominciaSpawn()
    {
        InvokeRepeating("SpawnTile", 1f, 0.25f); // richiama spawnsx dopo 2 sec e dopo ogni 0.1s

    }
    void CreaMoneta()
    {

        int rand = Random.Range(0, 10);
        if (rand < 3)
        {
            Instantiate(Gold, new Vector3(pos.x, pos.y + 0.75f, pos.z), Quaternion.identity);
        }

    }
    void CreaMacchina()
    {
        float corsiasx = Random.Range(-2f,-0.05f);
        float corsiadx = Random.Range(0.05f,2f);
        int rand = Random.Range(0, 1000);

        int spawnModel = Random.RandomRange(0,10);

        if (spawnModel < 2)
        {
            if (rand <= spawnCarRateSx)
            {
                Instantiate(Car, new Vector3(corsiasx, pos.y + 0.85f, pos.z), Car.transform.rotation);
            }
            else if (rand >= spawnCarRateDx)
                Instantiate(Car, new Vector3(corsiadx, pos.y + 0.85f, pos.z), Car.transform.rotation);
        }
        else if (spawnModel > 8)
        {
            if (rand <= spawnCarRateSx)
            {
                Instantiate(Car1, new Vector3(corsiasx, pos.y + 0.6f, pos.z), Car.transform.rotation);
            }
            else if (rand >= spawnCarRateDx)
                Instantiate(Car1, new Vector3(corsiadx, pos.y + 0.6f, pos.z), Car.transform.rotation);
        }
        else if (spawnModel >= 2 && spawnModel<= 5)
        {
            if (rand <= spawnCarRateSx)
            {
                Instantiate(Car2, new Vector3(corsiasx, pos.y + 0.7f, pos.z), Car.transform.rotation);
            }
            else if (rand >= spawnCarRateDx)
                Instantiate(Car2, new Vector3(corsiadx, pos.y + 0.7f, pos.z), Car.transform.rotation);
        }
    }
    void CreaBuca()
    {
        float bucax = Random.Range(-1.95f , 1.95f);
        
        //aggiungi effetto
        //aggiungi suono
        Instantiate(Buca, new Vector3(bucax, pos.y+0.5f, pos.z), Buca.transform.rotation);

    }

    void SpawnTile()
    {
        pos = ultimaPos;
        pos.z += size;
        ultimaPos = pos;
        
        Instantiate(RoadTile, pos, RoadTile.transform.rotation);

        CreaMoneta();
        CreaMacchina();
        CreaBuca();

    }
    

}

