﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyButton : MonoBehaviour
{
    public int carID;
    public Text buttonText;


    public void Buycar()

    {
        
        if(carID == 0)
        {
            Debug.Log("NO CAR ID SET!!!");
            return;
        }

        for(int i = 0; i< ShopSystem.shopSystem.carList.Count; i++)
        {
            if (ShopSystem.shopSystem.carList[i].carID == carID && !ShopSystem.shopSystem.carList[i].bought && (GameManager.gameManager.RequestMoney(ShopSystem.shopSystem.carList[i].carPrice)) && PlayerPrefs.GetInt("ModelloMacchina1") == 0)
            {


                //BUY THE CAR
                ShopSystem.shopSystem.carList[i].bought = true;
                PlayerPrefs.SetInt("ModelloMacchina1", 10);

                GameManager.gameManager.ReduceMoney(ShopSystem.shopSystem.carList[i].carPrice);
                UpdateBuyButton();
                


            }
            else if (ShopSystem.shopSystem.carList[i].carID == carID && !ShopSystem.shopSystem.carList[i].bought && (GameManager.gameManager.RequestMoney(ShopSystem.shopSystem.carList[i].carPrice)) && PlayerPrefs.GetInt("ModelloMacchina2") == 0)
            {

                //BUY THE CAR
                ShopSystem.shopSystem.carList[i].bought = true;
                PlayerPrefs.SetInt("ModelloMacchina2", 10);

                GameManager.gameManager.ReduceMoney(ShopSystem.shopSystem.carList[i].carPrice);
                UpdateBuyButton();
            
            }
            else if (ShopSystem.shopSystem.carList[i].carID == carID && !ShopSystem.shopSystem.carList[i].bought && (GameManager.gameManager.RequestMoney(ShopSystem.shopSystem.carList[i].carPrice)) && PlayerPrefs.GetInt("ModelloMacchina3") == 0)
            {

                //BUY THE CAR
                ShopSystem.shopSystem.carList[i].bought = true;
                PlayerPrefs.SetInt("ModelloMacchina3", 10);

                GameManager.gameManager.ReduceMoney(ShopSystem.shopSystem.carList[i].carPrice);
                UpdateBuyButton();
              
            }

            else if (ShopSystem.shopSystem.carList[i].carID == carID && !ShopSystem.shopSystem.carList[i].bought && !(GameManager.gameManager.RequestMoney(ShopSystem.shopSystem.carList[i].carPrice)))
            {
                Debug.Log("NOT ENOUGH MONEY!!");
            }
            else if (ShopSystem.shopSystem.carList[i].carID == carID && ShopSystem.shopSystem.carList[i].bought && PlayerPrefs.GetInt("ModelloMacchina1") > 0)
            {
                Debug.Log("Has BEEN BOUGHT ALREADY");
                UpdateBuyButton();
            }
        }
    }
    void UpdateBuyButton()
    {
        buttonText.text = "USING";
        for(int i = 0; i < ShopSystem.shopSystem.buyButtonList.Count; i++)
        {
            BuyButton buyButtonScript = ShopSystem.shopSystem.buyButtonList[i].GetComponent<BuyButton>();
            for(int j = 0; j< ShopSystem.shopSystem.carList.Count; j++)
            {
                //comprata, ma conflitto tra le macchine
                if(ShopSystem.shopSystem.carList[j].carID == buyButtonScript.carID && ShopSystem.shopSystem.carList[j].bought && ShopSystem.shopSystem.carList[j].carID != carID)
                {
                    buyButtonScript.buttonText.text = "use";
                }
            }

        }
    }

    



    
    
}
