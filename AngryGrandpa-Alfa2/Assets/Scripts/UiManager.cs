﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UiManager : MonoBehaviour {
    public GameObject StartPanel;
    public GameObject ShopPanel;
    public GameObject gameOverPanel;
    public GameObject inGamePanel;
    public GameObject cliccaText;
    public GameObject PunteggioText;
    public GameObject MonetaTotaleText;
    public Text MonetaTotale;
    public Text scoreInGame;
    public Text score;
    public Text MonetaScore;
    public Text highscoreStart;
    public Text highscoreGameOver;
    public static UiManager current;



    private void Awake()
    {
        if(current==null)
        {
            current = this;
        }
    }

    // Use this for initialization
    void Start () {
        if (PlayerPrefs.HasKey("highScore"))
            highscoreStart.text = "High Score: " + PlayerPrefs.GetInt("highScore") + "0m";
        else
            highscoreStart.text = " 0m";

        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GameStart()
    {
        cliccaText.GetComponent<Animator>().Play("cliccaTextDisappear");
        StartPanel.GetComponent<Animator>().Play("StartPanelDisappear");
        PunteggioText.SetActive(true);
        inGamePanel.SetActive(true);
    }

    public void GameOver()
    {
        
        gameOverPanel.SetActive(true);
        inGamePanel.SetActive(false);
        PunteggioText.SetActive(false);
        score.text = PlayerPrefs.GetInt("score").ToString() + "0m";
        highscoreStart.text = PlayerPrefs.GetInt("highScore").ToString() + "0m";
        highscoreGameOver.text = PlayerPrefs.GetInt("highScore").ToString() + "0m";
        MonetaScore.text = PlayerPrefs.GetInt("MoneteRaccolte").ToString() + "£";
        MonetaTotale.text = PlayerPrefs.GetInt("MoneteTotali").ToString() + "£";


    }

    public void Riparti() // script bottone restart
    {


        SceneManager.LoadScene(0); // con più scene posso mettere il nome della scena/il num della scena
        if (PlayerPrefs.GetInt("ModelloMacchina1") == 10)
        {
            SceneManager.LoadScene(1);
        }

    }

    public void apriShop() 
    {
       
        ShopPanel.SetActive(true);
        gameOverPanel.GetComponent<Animator>().Play("GameOverPanelDisappear1");
    }
    
    
}
