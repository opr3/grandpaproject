﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostCamera : MonoBehaviour
{

    
    public float speed;

    public GameObject ghost;
    Vector3 pos;

    bool partito;
    Rigidbody rb;
    bool GameOver;
    public static GhostCamera current;


    private void Awake()
    {
        rb = GetComponent<Rigidbody>(); // Mi serve per lavorare sul rigidbody 
        if (current == null)
        {
            current = this;
        }
    }

    void Start()
    {
        partito = false;
        GameOver = false;
        speed = 3;
    }


    void Update()
    {
        if (!partito)
        {
            if (Input.GetMouseButtonDown(0))
            {
                rb.velocity = new Vector3(pos.x, 0, speed);  // inizio su corsia sx
                partito = true;
            }
        }
        if (CarController.current.gameOver)
        {

            GameOver = true;
            rb.velocity = new Vector3(0, -10f, 0);

        }
        if (ScoreManager.current.score > 25)
        {
            
            GhostCamera.current.speed = 4;
        }

    }
}











