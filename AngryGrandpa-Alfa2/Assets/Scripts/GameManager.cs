﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{


    public static GameManager current;
    public static GameManager gameManager;
  
    public Text moneyText;

    void Awake()
    {

        if (current == null)
            current = this;
    }

    // Use this for initialization
    void Start()
    {
        gameManager = this;
        if (PlayerPrefs.HasKey("ModelloMacchina1") || PlayerPrefs.HasKey("ModelloMacchina2") || PlayerPrefs.HasKey("ModelloMacchina3"))
        {

            if (PlayerPrefs.GetInt("ModelloMacchina1") > 0)
            {
                Debug.Log("Has BEEN BOUGHT ALREADY");
            }
            else if (PlayerPrefs.GetInt("ModelloMacchina2") > 0)
            {
                Debug.Log("Has BEEN BOUGHT ALREADY");
            }
            else if (PlayerPrefs.GetInt("ModelloMacchina3") > 0)
            {
                Debug.Log("Has BEEN BOUGHT ALREADY");
            }

            else

                PlayerPrefs.SetInt("ModelloMacchina1", 0);
                PlayerPrefs.SetInt("ModelloMacchina2", 0);
            PlayerPrefs.SetInt("ModelloMacchina3", 0);
        }
       

    }

    // Update is called once per frame
    void Update()
    {
        UpdateUI();
    }

    public void StartGame()
    {
     
        UiManager.current.GameStart();
        ScoreManager.current.StartScore();
        
    }

    public void GameOver()
    {
        
        UiManager.current.GameOver();
        ScoreManager.current.StopScore();
    }

    public void AddMoney(int amount)
    {
        ScoreManager.current.MoneteTotali += amount;
        UpdateUI();
    }
    public void ReduceMoney(int amount)
    {
        PlayerPrefs.SetInt("MoneteTotali", PlayerPrefs.GetInt("MoneteTotali") - amount);
        UpdateUI();
    }

    public bool RequestMoney(int amount)
    {
        if (amount <= ScoreManager.current.MoneteTotali)
        {
            return true;
        }
        return false;
    }
    void UpdateUI()
    {
        moneyText.text = PlayerPrefs.GetInt("MoneteTotali").ToString() + "$";
    }
}
