﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RageBarScript : MonoBehaviour {
    public Slider ragebar;
    public int rabbia=0;
    public static RageBarScript current;

    private void Awake()
    {
        if(current == null)
        {
            current = this;
        }
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        Invoke("AumentaRage",1f);
        
    } 

    void AumentaRage()
    {
        if (rabbia < 10)
        {
            ragebar.value = Mathf.MoveTowards(ragebar.value, rabbia, 0.1f);
        }
        else if(rabbia == 10)
        {
            CarController.current.immortale();
             }
    }    
}
