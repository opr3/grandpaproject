﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject Ghost;
    Vector3 offset; // distanza palla cam
    public float lerpRate;
    public float speedCamera = 0;
    public bool gameOver;
    Rigidbody rb;
    public float slow = 0.995f;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>(); // Mi serve per lavorare sul rigidbody della macchina da qui
       
    }

    // Use this for initialization
    void Start()
    {
        //offset = Ghost.transform.position - transform.position; // il secondo TP si riferisce alla camera
        offset = new Vector3(transform.position.x, transform.position.y, speedCamera);
        gameOver = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (!gameOver)
        {
            InvokeRepeating("setSpeedCamera",0.1f,0.1f);
            rb.velocity = new Vector3(0, 0, speedCamera);

            //Segui();
        }
        else
            rb.position = new Vector3(transform.position.x,transform.position.y ,transform.position.z);
        
    }
    void setSpeedCamera()
    {
        if (CarController.current.partito)
            speedCamera = CarController.current.speed*slow;

    }
}
    

  
