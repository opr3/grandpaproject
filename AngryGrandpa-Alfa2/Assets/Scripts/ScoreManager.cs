﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{

    public static ScoreManager current;
    public int score;
    int MoneteRaccolte;
    public int MoneteTotali;
    public int MoneteTotaliFloat;
    bool sommato;
    public int count;
    int highScore;
    bool highScorePlayed;
    public AudioClip highScoreFx;
    //public AudioClip diamondFx;


    void Awake()
    {

        if (current == null)
            current = this;
    }

    // Use this for initialization
    void Start()
    {
        sommato = true;
        MoneteRaccolte = 0;
        score = 0;
        highScorePlayed = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void IncrementaScore()
    {

        score += 1;
        //mostro il nuovo punteggio nella label dei punti
        UiManager.current.scoreInGame.text = score.ToString() + "0m";

        //controllo se ha superato l'high score e in caso mostro l'animazione
        if (PlayerPrefs.HasKey("highScore") && !highScorePlayed)
        {

            if (score > PlayerPrefs.GetInt("highScore"))
            {

                //  AudioManager.current.PlaySound(highScoreFx);
                highScorePlayed = true;
            }


        }
    }

    public void MonetaScore()
    {
        MoneteRaccolte+=1;
        count++;
    }
   /* void MoneteTotaliCounter()
    {
        if(MoneteTotali = 0)
        {
            MoneteTotali = MoneteRaccolte;
        }
    } */
    
    public void StartScore()
    {

        InvokeRepeating("IncrementaScore", 0.1f, 0.5f);

    }

    public void StopScore()
    {

        CancelInvoke("IncrementaScore");
        CancelInvoke("MonetaScore");

        //registro l'ultimo score ottenuto
        PlayerPrefs.SetInt("score", score);


        PlayerPrefs.SetInt("MoneteRaccolte", MoneteRaccolte);
        //prova modello macchina

        



        //registro l'high score
        if (PlayerPrefs.HasKey("highScore"))
        {

            if (score > PlayerPrefs.GetInt("highScore"))
                PlayerPrefs.SetInt("highScore", score);
        }

        else
            PlayerPrefs.SetInt("highScore", score);


        if (PlayerPrefs.GetInt("MoneteTotali") == 0)
        {
            MoneteTotali = MoneteRaccolte;
            PlayerPrefs.SetInt("MoneteTotali", MoneteRaccolte);
        }

        else if (PlayerPrefs.GetInt("MoneteTotali") > 0 && sommato)
        {
            MoneteTotali = PlayerPrefs.GetInt("MoneteTotali") + PlayerPrefs.GetInt("MoneteRaccolte") ;
            sommato = false;

          
            PlayerPrefs.SetInt("MoneteTotali", MoneteTotali);
        }
    }
}
      
