﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {
    public static AudioManager current;
    
    private AudioSource audioSource;






    private void Awake()
    {
        if (current == null)
            current = this;
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void playSound(AudioClip clip)
    {
        audioSource = GetComponent<AudioSource>(); // 
        audioSource.clip = clip;
        audioSource.volume = 0.5f;
        audioSource.Play();
    }
    public void playSoundLow(AudioClip clip)
    {
        audioSource = GetComponent<AudioSource>(); // 
        audioSource.clip = clip;
        audioSource.volume = 0.2f;
        audioSource.Play();
    }
    public void playSoundHigh(AudioClip clip)
    {
        audioSource = GetComponent<AudioSource>(); // 
        audioSource.clip = clip;
        audioSource.volume = 1f;
        audioSource.Play();
    }
}
