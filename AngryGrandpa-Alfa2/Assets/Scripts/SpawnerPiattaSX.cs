﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerPiattaSX : MonoBehaviour {
    public GameObject EnviromentTile1;
    public GameObject EnviromentTile2;
    public GameObject EnviromentTile3;

    public GameObject Tree;
    public GameObject Tree1;
    public GameObject Tree2;
    


    Vector3 ultimaPos;
    Vector3 pos;


    float size;
    // Use this for initialization
    void Start () {
        ultimaPos = EnviromentTile1.transform.position;
        size = EnviromentTile1.transform.localScale.z;

        for (int i = 0; i < 100; i++) 
        {
            SpawnTileSX();
        }
        for (int i = 100; i < 200; i++) 
        {
            SpawnTileSX1();

        }
        for (int i = 200; i < 300; i++) 
        {
            SpawnTileSX2();

        }
    }

    public void CominciaSpawn()
    {
        InvokeRepeating("SpawnTileSX", 1f, 0.25f); // richiama spawnsx dopo 2 sec e dopo ogni 0.1s
        
    }
   
    
    void SpawnTileSX()
    {
        pos = ultimaPos;
        pos.z += size;
        ultimaPos = pos;
        Instantiate(EnviromentTile1, pos, Quaternion.identity);
        CreaAlbero();
    }
    void SpawnTileSX1()
    {
        pos = ultimaPos;
        pos.z += size;
        ultimaPos = pos;
        Instantiate(EnviromentTile2, pos, Quaternion.identity);
        CreaAlbero2();
    }
    void SpawnTileSX2()
    {
        pos = ultimaPos;
        pos.z += size;
        ultimaPos = pos;
        Instantiate(EnviromentTile3, pos, Quaternion.identity);
        CreaAlbero3();
    }

    void CreaAlbero()
    {
        float alberoPos = Random.Range(2.15f, 4.5f);

        Instantiate(Tree, new Vector3(alberoPos, pos.y + 0.5f, pos.z), Tree.transform.rotation);
    }

    void CreaAlbero2()
    {
        float alberoPos = Random.Range(2.15f, 4.5f);
        Instantiate(Tree1, new Vector3(alberoPos, pos.y + 0.5f, pos.z), Tree.transform.rotation);
    }

    void CreaAlbero3()
    {
        float alberoPos = Random.Range(2.15f, 4.5f);
        Instantiate(Tree2, new Vector3(alberoPos, pos.y + 0.5f, pos.z), Tree.transform.rotation);
    }
}
